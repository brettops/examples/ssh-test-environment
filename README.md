# ssh-test-environment

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

The test environment used for
"[Transfer files with ssh](https://brettops.io/blog/transfer-files-ssh/)".

![](environment.png)

## Prerequisites

- [Vagrant](https://developer.hashicorp.com/vagrant/downloads)

- [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

## Usage

Use `vagrant up` to spin up the environment.

```bash
vagrant up
```

Use `vagrant destroy` to clean up.

```bash
vagrant destroy
```
